# README #


### What is this repository for? ###

This repository contains terraform modules for deploying infrastructure to AWS.

Modules include:
    # VPC - Deploys a new Virtual Private Cloud in a chosen AWS Region. Contains public & private subnets, Internet access and peer link connectivity to a core VPC.
	# securitygroups - *
	# app_stack - Deploys a web server and an app server. Count of instances (and specification) is variable.
	# elb_app_stack - Deploys multiple web servers behind an Application load balancer

### How do I get set up? ###
Modules can be deployed directly from the repository by referencing the source URL in the Terraform configuration template.
