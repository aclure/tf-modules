provider "aws" {
  region = "eu-west-2"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

data "aws_availability_zones" "available" {}

/*
 * Create the New environment (peer)vpc
 */
resource "aws_vpc" "vpc" {
  cidr_block           = "${var.cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name        = "${var.environment}-vpc"
    environment = "${var.environment}"
  }
}

# Create DHCP option to add gps-cloud.local DNS server.
resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name_servers = ["10.220.1.10"]
}

resource "aws_vpc_dhcp_options_association" "dns_association" {
  vpc_id          = "${aws_vpc.vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dns_resolver.id}"
}

/* 
 * Create the subnets for the various resources.
*/
# Create VPC Subnet - web 1. Used for the NAT Gateway only.
resource "aws_subnet" "netaccesssubnet" {
  cidr_block        = "${var.netaccess}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name        = "${var.environment}-netaccess"
    environment = "${var.environment}"
  }
}

# create vpc subnet - public1
resource "aws_subnet" "publicsubnet1" {
  cidr_block        = "${var.public1}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name        = "${var.environment}-public1"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Public 2
resource "aws_subnet" "publicsubnet2" {
  cidr_block        = "${var.public2}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name        = "${var.environment}-public2"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Public 3
resource "aws_subnet" "publicsubnet3" {
  cidr_block        = "${var.public3}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"

  tags {
    Name        = "${var.environment}-public3"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Private 1
resource "aws_subnet" "privatesubnet1" {
  cidr_block        = "${var.private1}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name        = "${var.environment}-private1"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Private 2
resource "aws_subnet" "privatesubnet2" {
  cidr_block        = "${var.private2}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name        = "${var.environment}-private2"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Private 3
resource "aws_subnet" "privatesubnet3" {
  cidr_block        = "${var.private3}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"

  tags {
    Name        = "${var.environment}-private3"
    environment = "${var.environment}"
  }
}

# Internet access for the new Environment VPC. Creates Internet gateway, NAT Gateway and Elastic IP.
resource "aws_eip" "env_nateip" {
  vpc = true
}

resource "aws_internet_gateway" "env_gw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.gatewayname}"
    environment = "${var.environment}"
  }
}

resource "aws_nat_gateway" "env_nat" {
  allocation_id = "${aws_eip.env_nateip.id}"
  subnet_id     = "${aws_subnet.netaccesssubnet.id}"

  tags {
    Name        = "${var.environment}-natgw"
    environment = "${var.environment}"
  }
}

# To ensure that your NAT gateway can access the internet, 
# the route table associated with the subnet in which your NAT gateway resides must include 
# a route that points internet traffic to an internet gateway

/*
 * Create the PEERLINK.
 */
# Get details of the CORE (management) VPC
data "aws_vpc" "core_vpc" {
  filter {
    name   = "tag:Name"
    values = ["mgmt-vpc"]
  }
}

# Get details of the PUBLIC Route table in the CORE VPC
data "aws_route_table" "private" {
  filter {
    name   = "tag:Name"
    values = ["private"]
  }

  filter {
    name   = "tag:environment"
    values = ["management"]
  }
}

# Get details of the PRIVATE Route table in the CORE VPC
data "aws_route_table" "public" {
  filter {
    name   = "tag:Name"
    values = ["public"]
  }

  filter {
    name   = "tag:environment"
    values = ["management"]
  }
}

# Create the peer link between new VPC and the CORE (management) VPC
resource "aws_vpc_peering_connection" "peerlink" {
  vpc_id      = "${aws_vpc.vpc.id}"
  auto_accept = "${var.auto_accept}"
  peer_vpc_id = "${data.aws_vpc.core_vpc.id}" # Peer in this instance is for the peer link, so the remote VPC, which is the Management / CORE VPC.
}

/*
 * Create Routing Tables
 */
# Create the route table for the new VPC
resource "aws_route_table" "routetocore" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.core_cidr}"
    environment = "${var.environment}"
  }
}

resource "aws_route_table" "netaccess" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${var.environment}-netRT"
    environment = "${var.environment}"
  }
}

# routes for the net access route table
resource "aws_route" "netaccessdefault" {
  route_table_id         = "${aws_route_table.netaccess.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.env_gw.id}"
}

# Route to the Core/Management VPC
resource "aws_route" "coreroute" {
  route_table_id            = "${aws_route_table.routetocore.id}"
  destination_cidr_block    = "${var.core_cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
  depends_on                = ["aws_route_table.routetocore"]
}

# Route to the Internet for new VPC Subnets
resource "aws_route" "defaultRoute" {
  route_table_id         = "${aws_route_table.routetocore.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.env_nat.id}"
  depends_on             = ["aws_route_table.routetocore", "aws_nat_gateway.env_nat"]
}

# Route Table association for new (peer) VPC to the core management VPC. This adds the route table associations to the new Subnets.
resource "aws_route_table_association" "private1tocore" {
  route_table_id = "${aws_route_table.routetocore.id}" # Route for the private 1 subnet to the Core.
  subnet_id      = "${aws_subnet.privatesubnet1.id}"
}

resource "aws_route_table_association" "private2tocore" {
  route_table_id = "${aws_route_table.routetocore.id}" # Route for the private 2 subnet to the Core.
  subnet_id      = "${aws_subnet.privatesubnet2.id}"
}

resource "aws_route_table_association" "public1tocore" {
  route_table_id = "${aws_route_table.routetocore.id}" # Route for the public 1 subnet to the Core.
  subnet_id      = "${aws_subnet.publicsubnet1.id}"
}

resource "aws_route_table_association" "public2tocore" {
  route_table_id = "${aws_route_table.routetocore.id}" # Route for the public 2 subnet to the Core.
  subnet_id      = "${aws_subnet.publicsubnet2.id}"
}

resource "aws_route_table_association" "netaccess" {
  route_table_id = "${aws_route_table.netaccess.id}"  # Route table association for the netaccess subnet.
  subnet_id      = "${aws_subnet.netaccesssubnet.id}"
}

# Add a route to the new VPC from the PRIVATE Route Table in CORE VPC
resource "aws_route" "publiccoretoenv" {
  route_table_id            = "${data.aws_route_table.public.id}"
  destination_cidr_block    = "${var.cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
}

# Add a route to the new VPC from the PUBLIC Route Table in CORE VPC
resource "aws_route" "privatecoretoenv" {
  route_table_id            = "${data.aws_route_table.private.id}"
  destination_cidr_block    = "${var.cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
}

/*
 * Create Security Groups
 *
*/
resource "aws_security_group" "environment-SG" {
  name        = "${var.environment}-SG"
  description = "Allow ALL from/to local subnets."
  vpc_id      = "${aws_vpc.vpc.id}"

  tags {
    Name = "${var.environment}-SG"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# OUTPUTS
output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "sg_id" {
  value = "${aws_security_group.environment-SG.id}"
}

output "public_subnet_ids" {
  value = [
    "${aws_subnet.publicsubnet1.id}",
    "${aws_subnet.publicsubnet2.id}",
    "${aws_subnet.publicsubnet3.id}",
  ]
}

output "private_subnet_ids" {
  value = [
    "${aws_subnet.privatesubnet1.id}",
    "${aws_subnet.privatesubnet2.id}",
    "${aws_subnet.privatesubnet3.id}",
  ]
}

output "env_natgw_id" {
  value = "${aws_nat_gateway.env_nat.id}"
}

output "peerlink_id" {
  value = "${aws_vpc_peering_connection.peerlink.id}"
}

output "route_table_id" {
  value = "${aws_route_table.routetocore.id}"
}
