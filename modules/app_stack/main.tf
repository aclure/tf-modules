provider "aws" {
  region = "eu-west-2"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

data "terraform_remote_state" "env_vpc" {
  backend = "s3"

  config {
    bucket = "gps-aws-tf"
    key    = "${var.environment}/vpc/terraform.tfstate"
    region = "eu-west-2"
  }
}

# AMI for generic server deployments (includes Winrm, IIS, .NET & LAPS)
data "aws_ami" "ami" {
  most_recent = true
  owners      = ["477661052598"]

  filter {
    name   = "tag:role"
    values = ["web server"]
  }

  filter {
    name   = "tag:OS_Version"
    values = ["Windows"]
  }
}

/*
 * Deployment of EC2 instances
 *
*/
resource "aws_instance" "web_server" {
  ami                    = "${data.aws_ami.ami.id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.env_vpc.sg_id}"]
  instance_type          = "${var.web_instance_type}"
  subnet_id              = "${data.terraform_remote_state.env_vpc.public_subnet_ids[count.index]}"
  key_name               = "${var.key_name}"
  count                  = "${var.web_count}"

  user_data = <<-EOF
  <powershell>
  c:\windows\temp\configureansibleremoting -enableCredSSP
  </powershell>
  EOF

  tags {
    Name        = "${format("%.5s", var.stack_prefix)}_web${format("%02d", count.index + 1)}"
    environment = "${var.environment}"
    role        = "NewWeb"
    scheduler   = "default"
    stack       = "${format("%.5s", var.stack_prefix)}"
  }
}

resource "aws_instance" "app_server" {
  ami                    = "${data.aws_ami.ami.id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.env_vpc.sg_id}"]
  instance_type          = "${var.app_instance_type}"
  subnet_id              = "${data.terraform_remote_state.env_vpc.private_subnet_ids[0]}"
  key_name               = "${var.key_name}"
  count                  = "${var.app_count}"

  user_data = <<-EOF
  <powershell>
  c:\windows\temp\configureansibleremoting -enableCredSSP
  </powershell>
  EOF

  tags {
    Name        = "${format("%.5s", var.stack_prefix)}_app${format("%02d", count.index + 1)}"
    environment = "${var.environment}"
    role        = "NewApp"
    scheduler   = "default"
    stack       = "${format("%.5s", var.stack_prefix)}"
  }
}

/*
 * Null resources are used to launch the ansible playbook which configures the newly deployed instances. 
 * local-exec executes any command on the system wehere Terraform was run from. 
*/
resource "null_resource" "ansible-web" {
  triggers = {
    web_instance_ids = "${join(",", aws_instance.web_server.*.id)}"
  }

  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i /etc/ansible/ec2.py /etc/ansible/playbooks/web-svr.yml" # Command executed on linux instance with ansible installed. waits 300 secs to allow instance to initialise.
  }
}

resource "null_resource" "ansible-app" {
  triggers = {
    web_instance_ids = "${join(",", aws_instance.app_server.*.id)}"
  }

  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i /etc/ansible/ec2.py /etc/ansible/playbooks/app-svr.yml" # Command executed on linux instance with ansible installed. waits 300 secs to allow instance to initialise.
  }
}

# OUTPUTS. Used to provide details of the deployed instances and can be obtained from state file to use in other terraform modules.
output "web_ip" {
  description = "List of private IP addresses assigned to the web instances"
  value       = ["${aws_instance.web_server.*.private_ip}"]
}

output "app_ip" {
  description = "List of private IP addresses assigned to the App instances"
  value       = ["${aws_instance.app_server.*.private_ip}"]
}
