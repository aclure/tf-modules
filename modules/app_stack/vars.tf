# key pair for all server deployments
variable "key_name" {
  description = "the AWS key pair used to generate the default admin password"
}

# web server details
variable "web_instance_type" {
  description = "the size of ec2 instance for the web server. For example, t2.medium"
}

variable "web_count" {
  description = "The number of web servers to be deployed"
}

# App server details
variable "app_instance_type" {
  description = "the size of ec2 instance for the app server. For example, t2.medium"
}

variable "app_count" {
  description = "The number of app servers to be deployed"
}

# other variables
variable "environment" {
  type        = "string"
  description = "The name of the environment the instances will be deployed into. For example 'QA' or 'Dev'"
}

variable "stack_prefix" {
  type        = "string"
  description = "*** NOTE -This will be truncated to max of 5 characters. *** A descriptive name for the deployment of this module. For example, there could be several deployments of this module within the same VPC/Environment, this value will help to differentiate each deployment. If other modles are deployed as part of the same 'stack' (such as SQL) then these should have the same stack prefix."
}
