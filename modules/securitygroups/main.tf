variable "local_cidr" {
  default = ""
}

variable "peer_vpc_id" {
}
variable "core_vpc_id" {

}
/**
 * Security Groups
 */

#Security group for peer VPC - allow all in/out
resource "aws_security_group" "allow_all_local" {
  name = "allow_all_local"
  description = "Allow ALL from/to local subnets."
  vpc_id  = "${var.peer_vpc_id}"
  tags {
    Name = "any_aws_local"
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["${var.local_cidr}"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}