provider "aws" {
  region = "eu-west-2"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# Read the outputs from the Remote state in the S3 bucket.
data "terraform_remote_state" "env_vpc" {
  backend = "s3"

  config {
    bucket = "gps-aws-tf"
    key    = "${var.environment}/vpc/terraform.tfstate"
    region = "eu-west-2"
  }
}

# Get the names of all availability zones in the region
data "aws_availability_zones" "all" {

}

# Base AMI for all Win 2012R2 deployments
data "aws_ami" "ami" {
  most_recent = true
  owners      = ["477661052598"]
  filter {
    name   = "tag:role"
    values = ["web server"]
  }
  filter {
    name   = "tag:OS_Version"
    values = ["Windows"]
  }
}

/*
 * Create Security Groups
 *
*/
# general Security group for all instances being deployed. Allow all traffic local (anything to/from 10.0.0.0/8)
resource "aws_security_group" "environment-SG" {
  name        = "${var.environment}-SG"
  description = "Allow ALL from/to local subnets."
  vpc_id      = "${data.terraform_remote_state.env_vpc.vpc_id}"

  tags {
    Name = "${var.environment}-sg"
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["10.0.0.0/8"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Security Group for the Elastic Load Balancer. Allow the defined lb_port in from any 10.0.0.0/8.
resource "aws_security_group" "weblb-sg" {
  name          = "${var.environment}-weblbsg"
  description   = "security group for alb. Allow all in from local 10.0.0.0 subnets"
  vpc_id        = "${data.terraform_remote_state.env_vpc.vpc_id}"

  tags {
    Name        = "${var.environment}-sg"
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["10.0.0.0/8"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

/*
 * Deployment of EC2 instances
 *
*/
resource "aws_instance" "web_server" {
  ami                    = "${data.aws_ami.ami.id}"
  vpc_security_group_ids = ["${aws_security_group.environment-SG.id}"]
  instance_type          = "${var.web_instance_type}"
  subnet_id              = "${data.terraform_remote_state.env_vpc.public_subnet_ids[count.index]}"
  key_name               = "${var.key_name}"
  count                  = "${var.web_count}"
  user_data              = <<-EOF
  <powershell>
  c:\windows\temp\configureansibleremoting.ps1 -enableCredSSP
  </powershell>
  EOF

  tags {
    Name        = "${var.environment}_web_${format("%03d", count.index + 1)}"
    environment = "${var.environment}"
    role        = "NewWeb"
    scheduler   = "default"
  }
}

resource "aws_instance" "app_server" {
  ami                    = "${data.aws_ami.ami.id}"
  vpc_security_group_ids = ["${aws_security_group.environment-SG.id}"]
  instance_type          = "${var.app_instance_type}"
  subnet_id              = "${data.terraform_remote_state.env_vpc.private_subnet_ids[count.index]}"    # Gets the IDs of all subnets deployed via Remote state. count.index loops through all returned subnets to spread instances across them.
  key_name               = "${var.key_name}"
  count                  = "${var.app_count}"
  user_data              = <<-EOF
  <powershell>
  c:\windows\temp\configureansibleremoting.ps1 -enableCredSSP
  </powershell>
  EOF
 
  tags {
    Name        = "${var.environment}_app_${format("%03d", count.index + 1)}"
    environment = "${var.environment}"
    role        = "NewApp"
    scheduler   = "default"
  }
}

/*
 * Deployment of Application Load Balancer and associated resources (Target groups, listeners etc)
 *
*/
resource "aws_alb_target_group" "webtg" {
  name     = "${var.environment}-webtg"
  port     = "${var.web_instance_port}"
  protocol = "${var.web_instance_protocol}"
  vpc_id   = "${data.terraform_remote_state.env_vpc.vpc_id}"
  health_check {
    healthy_threshold   = 2                                 # Number of times health check has to succeed before marking instance as healthy
    unhealthy_threshold = 2                                 # Number of times health check has to succeed before marking instance as unhealthy
    port                = "${var.web_instance_port}"
    timeout             = 5                                  
    path                = "/healthcheck.html"
    interval            = 15                                # Interval between health check connection tests.
  }
}

resource "aws_alb_target_group_attachment" "webtg-ec2" {
  count             = "${var.web_count}"
  target_group_arn  = "${aws_alb_target_group.webtg.id}"
  target_id         =  "${element(aws_instance.web_server.*.id, count.index)}"
  port              = "${var.web_instance_port}"
}

resource "aws_alb" "webalb" {
  name            = "${var.environment}-webalb"
  internal        = "${var.LB_internal}"
  subnets         = ["${data.terraform_remote_state.env_vpc.public_subnet_ids}"]
  security_groups = ["${aws_security_group.weblb-sg.id}"]
}

resource "aws_alb_listener" "web-fe" {
  load_balancer_arn = "${aws_alb.webalb.id}"
  port              = "${var.lb_port}"
  protocol          = "${var.lb_protocol}"
  default_action {
    target_group_arn = "${aws_alb_target_group.webtg.id}"
    type             = "forward"
  }
}


/*
 * Null resources are used to launch the ansible playbook which configures the newly deployed instances. 
 * local-exec executes any command on the system wehere Terraform was run from. 
*/
resource "null_resource" "ansible-web" {
  triggers = {
    web_instance_ids = "${join(",", aws_instance.web_server.*.id)}"
  }
  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i /etc/ansible/ec2.py /etc/ansible/playbooks/web-svr.yml"         # Command executed on linux instance with ansible installed. waits 300 secs to allow instance to initialise.
  }
}

resource "null_resource" "ansible-app" {
  triggers = {
    web_instance_ids = "${join(",", aws_instance.app_server.*.id)}"
  }
  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i /etc/ansible/ec2.py /etc/ansible/playbooks/app-svr.yml"        # Command executed on linux instance with ansible installed. waits 300 secs to allow instance to initialise.
  }
}

/*
 * OUTPUTS. Used to provide details of the resources deployed.
 *
*/
output "web_ip" {
  description = "List of private IP addresses assigned to the web instances"
  value       = ["${aws_instance.web_server.*.private_ip}"]
}

output "app_ip" {
  description = "List of private IP addresses assigned to the App instances"
  value       = ["${aws_instance.app_server.*.private_ip}"]
}

output "alb_dns" {
  description = "DNS Hostname of the application load balancer"
  value       = ["${aws_alb.webalb.dns_name}"]
}

output "elb_securityGroup" {
  description = "Security Group ID for the Web ELB"
  value       = ["${aws_security_group.weblb-sg.id}"]
}
