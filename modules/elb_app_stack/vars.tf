# key pair for all server deployments
variable "key_name" {
  description = "the AWS key pair used to generate the default admin password"
}

# web server details
variable "web_instance_type" {
  description = "the size of ec2 instance for the web server. For example, t2.medium"
}

variable "web_count" {
  description = "The maximum number of web servers to be deployed in the ASG"
}

variable "lb_port" {
  description = "The port that the Load Balancer should listen on for incoming connections"
}

variable "lb_protocol" {
  description = "The protocol type for the incomoing traffic to the load balancer. Example, tcp"
}

variable "LB_internal" {
  description = "is the load balancer internal or internet facing?"
}

variable "web_instance_port" {
  description = "The port that the web server should listen on for incoming connections"
}

variable "web_instance_protocol" {
  description = "The protocol type for incoming connections to the web server. Example tcp."
}

# App server details
variable "app_instance_type" {
  description = "the size of ec2 instance for the app server. For example, t2.medium"
}

variable "app_count" {
  description = "The number of app servers to be deployed"
}

# other variables
variable "environment" {
  description = "The name of the environment the instances will be deployed into. For example 'QA' or 'Dev'"
}


