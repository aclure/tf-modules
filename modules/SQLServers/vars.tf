# key pair for all server deployments
variable "key_name" {
  description = "the AWS key pair used to generate the default admin password"
}

# db instance details
variable "db_instance_type" {
  description = "the size of instance to be deployed. For example T2.medium"
  default     = "t2.medium"
}

variable "db_count" {
  description = "The number of db servers to be deployed"
  default     = "1"
}

# other variables
variable "environment" {
  description = "name of the environment where the instance will be deployed. For example 'QA' or 'dev'"
}

variable "stack_prefix" {
  type        = "string"
  description = "*** NOTE -This will be truncated to max of 5 characters. *** A descriptive name for the deployment of this module. For example, there could be several deployments of this module within the same VPC/Environment, this value will help to differentiate each deployment. If other modles are deployed as part of the same 'stack' (such as SQL) then these should have the same stack prefix."
}
