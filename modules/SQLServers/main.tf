provider "aws" {
  region = "eu-west-2"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

/*
 * This block finds the private GPS ami for SQL Server deployments (Windows Server 2008R2 with SQL Server installed using MSDN SQL license).
    Other filters should probably be added to this block. Maybe environment or SQL version.
 */
data "aws_ami" "db_ami" {
  most_recent = true             # Where multiple AMIs are found that match the other filters, the most recent will be used.
  owners      = ["477661052598"] # will restrict the search to only AMIs owned by this account (GPS-Dev)

  filter {
    name   = "tag:role"     # Filters the retunred AMIs using the assigned tags. In this case the tag name is 'role'. All AMIs should have a role tag defined.
    values = ["sql server"] # This is the value of the 'role' tag being searched for.
  }

  filter {
    name   = "tag:OS_Version" # An additional filter. In this case another tag named 'OS_Version'. All AMIs should have this tag defined.
    values = ["Windows"]      # Only AMis with 'Windows' as the OS_Version tag will be returned.
  }
}

data "terraform_remote_state" "env_vpc" {
  backend = "s3"

  config {
    bucket = "gps-aws-tf"
    key    = "${var.environment}/vpc/terraform.tfstate"
    region = "eu-west-2"
  }
}

# Resources to be deployed
resource "aws_instance" "db_server" {
  ami                    = "${data.aws_ami.db_ami.id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.env_vpc.sg_id}"]
  instance_type          = "${var.db_instance_type}"
  subnet_id              = "${data.terraform_remote_state.env_vpc.private_subnet_ids[0]}"
  key_name               = "${var.key_name}"
  count                  = "${var.db_count}"

  # user_data               = "${data.template_file.init.rendered}"

  tags {
    Name         = "${format("%.5s", var.stack_prefix)}_db${format("%02d", count.index + 1)}"
    environment  = "${var.environment}"
    role         = "db server"
    stack_prefix = "${var.stack_prefix}"
  }
}
